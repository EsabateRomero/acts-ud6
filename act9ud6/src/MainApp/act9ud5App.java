/**
 * 
 */
package MainApp;

import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act9ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Random random = new Random();
		Scanner teclat = new Scanner(System.in);
		int tamanoArray = tamanoArray(teclat);
		int[] array = crearArray(tamanoArray, random);
		System.out.println("La suma total �s: "+ mostrarArraySuma(array));
	}
	
	public static int mostrarArraySuma(int[] array) {
		int contador=0, suma=0;
		while(contador<array.length) {
			System.out.println("En la posici�n " + contador +" hay el valor " + array[contador]);
			suma = suma + array[contador];
			contador++;
		}
		return suma;
	}
	
	public static int[] crearArray(int tamanoArray, Random random) {
		int[] array = new int[tamanoArray];
		int contador=0;
		while(contador<array.length) {
			array[contador] = random.nextInt(10);
			contador++;
		}
		return array;
	}
	
	public static int tamanoArray(Scanner teclat) {
		boolean numValid = false;
		int tamano = 0;
		while(!numValid) {
			System.out.println("Introduce el tama�o deseado para tu array: ");
			tamano = teclat.nextInt();
			if(tamano>0)
				numValid=true;
		}
		
		return tamano;	
		
	}
}
