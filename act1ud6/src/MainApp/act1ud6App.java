/**
 * 
 */
package MainApp;

import java.util.Scanner;

/**
 * @author Elisabet Sabat�
 *
 */
public class act1ud6App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		
		switch (elegirOpcion(teclat)) {
		case 1:
			double areaCirculo = areaCirculo(teclat);
			System.out.println("L'�rea del cercle que heu demanat �s " + areaCirculo);
			break;
		case 2:
			double areaCuadrado = areaCuadrado(teclat);
			System.out.println("L'�rea del quadrat que heu demanat �s " + areaCuadrado);
			break;
		case 3:
			double areaTriangulo = areaTriangulo(teclat);
			System.out.println("L'�rea del triangle que heu demanat �s " + areaTriangulo);
			break;

		}

	}
	
	public static double areaCirculo(Scanner teclat) {
		System.out.println("Per a calcular l'�rea del cercle necessitem coneixer el radi");
		System.out.println("Radi: ");
		double radi = teclat.nextDouble();
		double area = (radi*radi)*Math.PI;
		return area;
	}
	
	public static double areaTriangulo(Scanner teclat) {
		System.out.println("Per a calcular l'�rea del triangle necessitem coneixer la base i l'altura");
		System.out.println("Base: ");
		double base = teclat.nextDouble();
		System.out.println("Altura: ");
		double altura = teclat.nextDouble();
		double area = (base*altura)/2;
		return area;
	}
	
	public static double areaCuadrado(Scanner teclat) {
		System.out.println("Per a calcular l'�rea del quadrat necessitem coneixer quant medeix un costat");
		System.out.println("Costat: ");
		double costat = teclat.nextDouble();
		double area = costat*costat;
		return area;
	}

	
	public static int elegirOpcion(Scanner teclat) {
		boolean opcioValida=false;
		int opcio = 5;
		while(!opcioValida) {
		System.out.println("1. Calcular area del c�rculo:");
		System.out.println("2. Calcular area del cuadrado:");
		System.out.println("3. Calcular area del triangulo:");
		
		System.out.println("----");
		System.out.println("TRIA UNA OPCI�:");
		opcio = teclat.nextInt();
		
		if(opcio==1|opcio==2|opcio==3)
			opcioValida = true;
		else
			System.out.println("La opci� seleccionada no est� disponible");

		}
		
		return opcio;
		}
		

}
