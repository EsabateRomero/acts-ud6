/**
 * 
 */
package MainApp;

import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act10ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Random random = new Random();
		Scanner teclat = new Scanner(System.in);
		boolean posicionsValides = false;
		int numPosicions = 0;
		
		while(!posicionsValides) {
			System.out.println("Quantes posicions vol dins de l'array?");
			numPosicions = teclat.nextInt();
			if(numPosicions>=1)
				posicionsValides = true;
			else
				System.out.println("Per a construir un array necessitem com a m�nim 1");
		}
		
		System.out.println("\n\nAra hem de decidir dins de quin rang volem els nombres aleatoris!");
		
		int[] array = parametres(numPosicions, random, teclat);
		mostrarValors(array);
		
		System.out.println("\n\nEl nombre m�s gran que ha aparegut �s " + trobarValorMesGran(array));
		
	}
	
	public static int[] parametres(int numPosicions, Random random, Scanner teclat) {
		
		int contador = 0;
		int[] array = new int[numPosicions];
		
		System.out.println("parametre 1:");
		int param1 = teclat.nextInt();
		System.out.println("parametre 2:");	
		int param2 = teclat.nextInt();
		
		if(param2>param1) {
			while(contador<numPosicions){
				array[contador] = random.nextInt(param2+1-param1) + param1;
				if(numPrimo(array[contador]))
						contador++;
			}
		} else
			System.out.println("-- Heu d'afegir primer el valor m�nim i despr�s el m�xim.");
			
		return array;
	}
	
	public static void mostrarValors(int[] array) {
		int contador = 0;
		System.out.println("\n\n-- CONTINGUT DEL ARRAY: --------");
		while(contador<array.length){
			System.out.println("\n--> array["+contador+"] = " + array[contador]);
			contador++;
		}
	}
	
	public static int trobarValorMesGran(int[] array) {
		int contador=0;
		int mesGran = 0;
		while(contador<array.length) {
			if(array[contador]>mesGran)
				mesGran = array[contador];
			contador++;
		}
		return mesGran;
	}
	
	public static boolean numPrimo(int numero) {
		
		int contador = 2;
		boolean numPrimer = true;
		while(contador!=numero) {
			if(numero%contador==0)
				numPrimer = false;
			contador++;
		}
		return numPrimer;
	}
}
