/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act7ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Por favor, introduzca la cantidad de euros a convertir:");
		double euros = teclat.nextInt();
		
		mostrarConversio(euros, elegirConversion(teclat));
		
		
	}
	public static void mostrarConversio(double euros, String tipoConversion) {
		double monedaConversio = 0;
		switch (tipoConversion) {
		case "dolares":
			monedaConversio = 1.28611*euros;
			break;
		case "yenes":
			monedaConversio = 129.852*euros;
			break;
		case "libras":
			monedaConversio = 0.85*euros;
			break;

		}
		System.out.println("Cambio realitzado. " );
		
		System.out.println("Son " + monedaConversio +" de "+tipoConversion);
		
		
	}
	
	public static String elegirConversion(Scanner teclat) {
		boolean opcioValida = false;
		String opcio = "";
		while(!opcioValida) {
		System.out.println("--> 1. Convertir a dolares");
		System.out.println("--> 2. Convertir a yenes");
		System.out.println("--> 3. Convertir a libras");
		
		opcio = teclat.next();
		
		if(opcio.equalsIgnoreCase("dolares")||opcio.equalsIgnoreCase("yenes")||opcio.equalsIgnoreCase("libras"))
			opcioValida =  true;
		else
			System.out.println("La opcion indroducida no es valida");
		}
		return opcio;
	}
}
