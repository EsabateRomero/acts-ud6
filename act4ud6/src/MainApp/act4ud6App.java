/**
 * 
 */
package MainApp;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Elisabet Sabat�
 *
 */
public class act4ud6App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Creaci� de un factorial");
		int factorial = 1, contador = 1, numero = numeroValid();
		System.out.println("El numero factorial de " +numero+ " es calcula amb ");
		while(contador<=numero) {
			if(contador!=1)
				System.out.print(" * ");
				System.out.print(contador);
			factorial = factorial*contador;
			contador++;
		}
		System.out.println(" = " + factorial);
	}
	
	public static int numeroValid() {
		Scanner teclat = new Scanner(System.in);
		boolean numValid = false;
		int numero = 0;
		while(!numValid) {
			System.out.println("Introdueix el numero");
			numero = teclat.nextInt();
		if(numero>0)
			numValid = true;
		}
		
		return numero;
	}

}
