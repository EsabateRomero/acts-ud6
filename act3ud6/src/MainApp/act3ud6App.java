/**
 * 
 */
package MainApp;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Elisabet Sabat�
 *
 */
public class act3ud6App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Digueu-me el n�mero a comprobar: ");
		int numero = teclat.nextInt();
		
		if(numPrimer(numero))
			System.out.println("El seu n�mero es primer");
		else
			System.out.println("El seu n�mero no �s primer");
	}
	
	public static boolean numPrimer(int numero) {
		
		int contador = 2;
		boolean numPrimer = true;
		while(contador!=numero) {
			if(numero%contador==0)
				numPrimer = false;
			contador++;
		}
		return numPrimer;
	}

}
