/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act6ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Introdueix un n�mero sencer i et direm les xifres que t�");
		int numero = teclat.nextInt();
		System.out.println("El vostre nombre t� "+ contarXifres(numero) + " xifres.");
		
	}
	
	public static int contarXifres(int numero) {
		String xifres = String.valueOf(numero);
		return xifres.length();
	}
}
