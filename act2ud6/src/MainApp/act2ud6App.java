/**
 * 
 */
package MainApp;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Elisabet Sabat�
 *
 */
public class act2ud6App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Random random = new Random();
		boolean parametresIntroduits = false;
		Scanner teclat = new Scanner(System.in);
		int[] parametres = new int[2];
		System.out.println("Quants n�meros sencers aleatoris voleu generar?");
		int quantitatAleatoris = teclat.nextInt();
		
		while(!parametresIntroduits) {
			System.out.println("Sisplau, introdu�u els parametres");
			parametres = parametres(teclat);
			if(parametres[0]!=0)
				parametresIntroduits = true;		
		}
		
		int[] numsRandoms = generarAleatoris(parametres, quantitatAleatoris, random);
		System.out.println("----------------------------");
		System.out.println(" --> Voleu un total de "+quantitatAleatoris+ " elements dins de l'array");
		System.out.println(" --> Voleu els n�mero aleatoris entre " + parametres[0] +" i " + parametres[1]);
		System.out.println("------------------");
		mostrarArray(numsRandoms);
		
		}
	
	public static void mostrarArray(int[] array) {
		int contador= 0;
		while(contador<array.length) {
			System.out.println("array["+contador+"] = "+array[contador]);
			contador++;			
		}
	}
	public static int[] generarAleatoris(int[] parametres, int quantitatAleatoris, Random random) {
		int contador = 0;
		int[] numsRandoms = new int[quantitatAleatoris];
		while(contador<quantitatAleatoris) {
			numsRandoms[contador]=random.nextInt(parametres[1]+1-parametres[0]) + parametres[0];
			contador++;	
		}	
		return numsRandoms;	
	}
	
	public static int[] parametres(Scanner teclat) {
		
		int[] parametres = new int[2];
		System.out.println("parametre 1:");
		int param1 = teclat.nextInt();
		System.out.println("parametre 2:");	
		int param2 = teclat.nextInt();
		if(param2>param1) {
			parametres[0] = param1;
			parametres[1] = param2;
		} else
			System.out.println("-- Heu d'afegir primer el n�mero m�nim i despr�s el m�xim");
		return parametres;
	}
		

}
