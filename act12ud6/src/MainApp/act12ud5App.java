/**
 * 
 */
package MainApp;

import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act12ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		Random random = new Random();
		System.out.println("Afegeix les dimensions del array");
		int dimension = teclat.nextInt();
		int[] array = crearArray(dimension, random);
		System.out.println("\n-- PRIMER ARRAY GENERAT --");
		mostrarArray(array);
		System.out.println("\n\nEn quin n�mero vols que acabin els n�meros que"
				+ " passaran a un altre array?");
		int ultimDigit = teclat.nextInt();
		int[] arrayUltimDigit = buscarAcabats(array, ultimDigit);
		
		System.out.println("\n-- SEGON ARRAY GENERAT --");
		mostrarArray(arrayUltimDigit);
		
	}
	
	public static int quantitatAcabats(int[] array, int ultimDigit) {
		int contador = 0;
		int quantitat= 0;
		while (contador<array.length) {
			if(array[contador]%10==ultimDigit)
				quantitat++;
			contador++;
		}
		return quantitat;
	}
	
	public static int[] buscarAcabats(int[] array, int ultimDigit) {
		int contador=0, contador2=0;
		int[] arrayUltimDigit = new int[quantitatAcabats(array, ultimDigit)];
		while(contador<array.length) {
			
			if(array[contador]%10==ultimDigit) {
				arrayUltimDigit[contador2]=array[contador];
				contador2++;				
			}
			contador++;
		}
		return arrayUltimDigit;
	}
	
	public static void mostrarArray(int[] array) {
		int contador= 0;
		while(contador<array.length) {
			System.out.println("array["+contador+"] = "+array[contador]);
			contador++;			
		}
	}
	
	public static int[] crearArray(int dimension, Random random) {
		
		int[] array = new int[dimension];
		int contador = 0;
		while (contador<array.length) {
			array[contador] = random.nextInt(301);
			contador++;
		}
		return array;
	}
}
