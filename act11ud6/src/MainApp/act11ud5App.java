/**
 * 
 */
package MainApp;

import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act11ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		Random random = new Random();
		System.out.println("Afegim dimensi� del array : ");
		int dimension = teclat.nextInt();
		System.out.println("Primer array");
		int[] array = crearArray(dimension, random);
		mostrarArray(array);
		System.out.println("Segon array");
		int[] array2 = array;
		mostrarArray(array2);
		System.out.println("Array combinat");
		int[] arrayCombinat = combinarArrays(array, array2);
		mostrarArray(arrayCombinat);
		
	}
	
	public static int[] combinarArrays(int[] array, int[] array2) {
		
		int[] arrayCombinat = new int[array.length]; 
		int contador = 0;
		while(contador<arrayCombinat.length) {
			arrayCombinat[contador] = array[contador]*array2[contador];
			contador++;
		}
		return arrayCombinat;
	}
	
	public static void mostrarArray(int[] array) {
		int contador= 0;
		while(contador<array.length) {
			System.out.println("array[" + contador+ "] = " + array[contador]);
			contador++;
		}
	}
	
	public static int[] crearArray(int dimension, Random random) {
		
		int[] array = new int[dimension];
		int contador = 0;
		while (contador<array.length) {
			array[contador] = random.nextInt(9);
			contador++;
		}
		return array;
	}
}
