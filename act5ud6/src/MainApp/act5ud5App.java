/**
 * 
 */
package MainApp;

import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Elisabet Sabat�
 *
 */
public class act5ud5App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner teclat = new Scanner(System.in);
		System.out.println("Introdueix un n�mero decimal per a poder convertir-lo en binari:");
		Double numeroIntroduit = teclat.nextDouble();
		String numBinari = conversioDeNum(numeroIntroduit);
		System.out.println("resultat: " + numBinari);
		
	}
	
	public static String conversioDeNum(Double numero) {
		String numConversio = "";
		while(numero>1) {
			numConversio = (int)(numero%2) + numConversio;
			numero = numero/2;
		}
		return numConversio;
	}
	
}
